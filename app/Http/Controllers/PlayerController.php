<?php

namespace App\Http\Controllers;

use App\NbaRequest;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use JasonRoman\NbaApi\Request\Data\Prod\Teams\TeamsRequest;
use JasonRoman\NbaApi\Request\Stats\Stats\Roster\TeamRosterRequest;
use JasonRoman\NbaApi\Request\Data\MobileTeams\Player\PlayerCardRequest;
use JasonRoman\NbaApi\Request\Data\Prod\Player\PlayerProfileRequest;
use JasonRoman\NbaApi\Request\Data\Prod\Roster\LeagueRosterPlayersRequest;

class PlayerController extends Controller
{
    public function index(){
        dd('until next timeeeee');
        // prepare the request
        $request = LeagueRosterPlayersRequest::fromArray([
            'year' => 2018
        ]);

        //use global method the fetch data
        $client = new NbaRequest();

        $datas = $client->handleRequest($request);
        //dd($datas);
        // wrap up results into pagination
        $result = $this->paginate($datas->league->standard);

        return view('players')->with('datas',$result);
    }

    public function showPlayer($id){

        // prepare the request
        $request_ = PlayerProfileRequest::fromArray([
            'year' => 2018,
            'playerId' => intval($id)
        ]);

        //use global method the fetch data
        $client = new NbaRequest();

        $datas = $client->handleRequest($request_);

        $statistics = $datas->league->standard->stats->latest;

        // get player bio
        $request2 = PlayerCardRequest::fromArray([
            'format' => 'json',
            'leagueSlug' => 'nba',
            'year' => 2018,
            'playerId' => intval($id),
            'seasonTypeCode' => '01'
        ]);

        $playerBio = $client->handleRequest($request2);

        return view('playerBio', compact('statistics','playerBio'));


    }

    public function showTeam($id){

        $request = TeamRosterRequest::fromArray([
            'season' => '2018-19',
            'teamId' => intval($id),
            'leagueId' => '00'
        ]);

        $client = new NbaRequest();

        $teamPlayers = $client->handleRequest($request);

        $request2 = TeamsRequest::fromArray([
           'year' => 2018
        ]);

        $teams = $client->handleRequest($request2);

        $teamDatas = [];

        foreach($teams->league->standard as $team){
            if($team->isNBAFranchise && $team->teamId == $id){
                $teamDatas['city'] = $team->city;
                $teamDatas['name'] = $team->fullName;
                $teamDatas['confName'] = $team->confName;
                $teamDatas['divName'] = $team->divName;
            }
        }

        return view('teamRoster',compact('teamPlayers', 'teamDatas'));
    }

    public function paginate($items, $perPage = 10, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }
}
