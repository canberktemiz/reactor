<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use JasonRoman\NbaApi\Client\Client;

class NbaRequest extends Model
{
    function handleRequest($request){
        $clientNba = new Client();

        $response = $clientNba->request($request);

        // convert to array to display in views
        $datas = $response->getObjectFromJson();

        return $datas;
    }
}
