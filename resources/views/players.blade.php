@extends('layouts.master')

@section('content')

    <div class="container">
            <div class="row">
                <h2>Player Pool</h2>
            </div>

            <div class="row">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">First Name</th>
                            <th scope="col">Last Name</th>
                            <th scope="col">Position</th>
                        </tr>
                    </thead>
                    <tbody>

                    @foreach($datas as $data)
                        <form id="target-{{$data->personId}}" action="/player/{{$data->personId}}" method="post">
                            @csrf
                            <tr>
                                <input type="hidden" name="id" value="{{$data->personId}}">
                                <td>{{$data->jersey}}</td>
                                <td><a href="#" class="bio" data-id="{{$data->personId}}">{{$data->firstName}}</a></td>
                                <td><a href="#" class="bio" data-id="{{$data->personId}}">{{$data->lastName}}</a></td>
                                <td>{{$data->pos}}</td>
                            </tr>
                        </form>
                    @endforeach

                    </tbody>
                </table>
                <nav aria-label="Page navigation example">
                    <ul class="pagination justify-content-center">
                        {{ $datas->links() }}
                    </ul>
                </nav>
            </div>
    </div>

    <script>

        $('.bio').on('click',function(){
            const playerId = $(this).data('id');
            $('#target-' + playerId).submit();
        })
    </script>

@endsection