@extends('layouts.master')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-4">
                <img src="https://nba-players.herokuapp.com/players/{{$playerBio->pl->ln}}/{{$playerBio->pl->fn}}" alt="img" width="350px" height="254">
            </div>
            <div class="col-8">
                <h3>{{$playerBio->pl->fn}} {{$playerBio->pl->ln}}</h3>
                <p>#{{$playerBio->pl->num}} {{$playerBio->pl->pos}} | {{$playerBio->pl->ht}}" {{$playerBio->pl->wt}}Ibs | {{$playerBio->pl->tn}}</p>
                <p>Born: {{$playerBio->pl->dob}}</p>
                <p>Experience: {{$playerBio->pl->y}}</p>
                <p>Collage: {{$playerBio->pl->hcc}}</p>

                <form id="sendTeamId" action="/team/{{$playerBio->pl->tid}}" method="post">
                    @csrf
                    <input type="hidden" name="teamId" value="{{$playerBio->pl->tid}}">
                    <a href="#">{{$playerBio->pl->tn}}</a>
                </form>

            </div>
        </div>
        <div class="row">
            <table class="table">
                <thead>
                    <tr>
                        <th>PPG</th>
                        <th>Rebound</th>
                        <th>Assist</th>
                        <th>Minutes</th>
                        <th>Turnover</th>
                        <th>Steal</th>
                        <th>Block</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>{{$statistics->ppg}}</th>
                        <th>{{$statistics->rpg}}</th>
                        <th>{{$statistics->apg}}</th>
                        <th>{{$statistics->mpg}}</th>
                        <th>{{$statistics->topg}}</th>
                        <th>{{$statistics->spg}}</th>
                        <th>{{$statistics->bpg}}</th>
                    </tr>
                </tbody>
            </table>
            <a href="/"><button type="button" class="btn btn-primary">Back</button></a>
        </div>
    </div>

    <script>
        $('a').on('click',function(){
            $('#sendTeamId').submit();
        });
    </script>
@endsection