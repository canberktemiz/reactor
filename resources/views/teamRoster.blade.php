@extends('layouts.master')

@section('content')

    <div class="container">
        <h1>{{$teamDatas['name']}} |  {{$teamDatas['confName']}} |  {{$teamDatas['divName']}}</h1>
        <table id="table_id" class="display">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Name</th>
                    <th>Pos</th>
                    <th>Age</th>
                    <th>HT</th>
                    <th>WT</th>
                    <th>Collage</th>
                </tr>
            </thead>
            <tbody>
                @foreach($teamPlayers->resultSets[0]->rowSet as $data)
                    <tr>
                        <td>{{$data[4]}}</td>
                        <form id="target-{{$data[12]}}" action="/player/{{$data[12]}}" method="post">
                            @csrf
                            <input type="hidden" value="{{$data[12]}}">
                            <td><a href="#" data-id="{{$data[12]}}">{{$data[3]}}</a></td>
                        </form>
                        <td>{{$data[5]}}</td>
                        <td>{{$data[9]}}</td>
                        <td>{{$data[6]}}</td>
                        <td>{{$data[7]}}</td>
                        <td>{{$data[11]}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <script>
        $(document).ready( function () {
            $('#table_id').DataTable();
        } );

        $('a').on('click',function(){
            const playerId = $(this).data('id');
            $('#target-' + playerId).submit();
        });
    </script>
@endsection