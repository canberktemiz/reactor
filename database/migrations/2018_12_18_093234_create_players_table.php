<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlayersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('players', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('personId');
            $table->unsignedInteger('team_id');
            $table->string('firstName');
            $table->string('lastName');
            $table->string('jersey');
            $table->string('position');
            $table->timestamp('dob');
            $table->string('yearsPro');
            $table->string('heightMeters');
            $table->string('weightKilograms');
            $table->string('collegeName');
            $table->string('country');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('players');
    }
}
