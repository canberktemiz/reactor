<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use JasonRoman\NbaApi\Client\Client;
use JasonRoman\NbaApi\Request\Data\Prod\Roster\LeagueRosterPlayersRequest;


class PlayerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $request = LeagueRosterPlayersRequest::fromArray([
            'year' => 2018
        ]);

        $clientNba = new Client();

        $response = $clientNba->request($request);

        // convert to array to display in views
        $datas = $response->getObjectFromJson();


        foreach($datas->league->standard as $key => $data){

                DB::table('players')->insert([
                    'personId' => $data->personId,
                    'team_id' => $data->teamId,
                    'firstName' => $data->firstName,
                    'lastName' => $data->lastName,
                    'jersey' => $data->jersey,
                    'position' => $data->pos,
                    'dob' => $data->dateOfBirthUTC ? $data->dateOfBirthUTC : date("Y-m-d H:i:s"),
                    'yearsPro' => $data->yearsPro,
                    'heightMeters' => $data->heightMeters,
                    'weightKilograms' => $data->weightKilograms,
                    'collegeName' => $data->collegeName,
                    'country' => $data->country,
                ]);




        }
    }
}
