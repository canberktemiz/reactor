<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use JasonRoman\NbaApi\Client\Client;
use JasonRoman\NbaApi\Request\Data\Prod\Teams\TeamsRequest;


class TeamTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $request = TeamsRequest::fromArray([
            'year' => 2018
        ]);

        $clientNba = new Client();

        $response = $clientNba->request($request);

        // convert to array to display in views
        $datas = $response->getObjectFromJson();


        foreach($datas->league->standard as $key => $data){

                DB::table('teams')->insert([
                    'teamId' => $data->teamId,
                    'city' => $data->city,
                    'confName' => $data->confName,
                    'divName' => $data->divName,
                    'fullName' => $data->fullName
                ]);




        }
    }
}
